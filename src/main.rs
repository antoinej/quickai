use hyper::body::Buf;
use hyper::{header, Body, Client, Request};
use serde_derive::{Deserialize, Serialize};
use spinners::{Spinner, Spinners};
use std::env;
use std::io::{Stdin, Stdout, Write};

#[derive(Deserialize, Debug)]
struct OAIChoices {
    text: String,
    index: u8,
    log_probs: Option<u8>,
    finish_reason: String
}

#[derive(Deserialize, Debug)]
struct OAIResponse {
    id: Option<String>,
    object: Option<String>,
    created: Option<u64>,
    model: Option<String>,
    choices: Vec<OAIChoices>
}

#[derive(Deserialize, Debug)]
struct OAIRequest {
    prompt: String,
    max_tokens: u32
}


fn main() {
    println!("Hello, world!");
}
